import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { Button, StyleSheet, Text, View, SafeAreaView } from 'react-native';
import { createServer } from 'miragejs';
import Moment from 'moment';
import StatusbarView from './components/statusbar/statusbar';
import TransactionListView from './components/transactionlist/transactionlist';

if (window.server) {
  server.shutdown();
}

window.server = createServer({
  routes() {
    Moment.locale('de');

    const account = {
      available: '250000.43',
      limit: '300002.00',
      transactions: [
        { name: 'REWE MARKT', date: '23.05.2021', amount: '142.34' },
      ]
    };

    function getCurrentDate() {
      let date = Moment(Date()).format('DD.MM.YYYY');
      console.log(date);
      return date;
    }

    this.get("/api/account", () => {

      const randomKey = parseInt(Math.random() * 100);
      const randomAmount = (Math.random() * 1000).toFixed(2);
      const newTransaction = { name: randomKey, date: getCurrentDate(), amount: randomAmount };
      const newAvailableBalance = (parseFloat(account.available) - parseFloat(newTransaction.amount)).toFixed(2).toString();

      account.available = newAvailableBalance;
      account.transactions = [...account.transactions, newTransaction];

      return account
    })
  },
})

export default function App() {

  const [account, updateAccount] = useState({
    available: '00.00',
    limit: '00.00',
    transactions: []
  });

  function fetchData() {
    fetch('api/account')
      .then((res) => res.json())
      .then((json) => updateAccount(json))
  }

  React.useEffect(() => {
    fetchData();
  }, [])

  return (
    <SafeAreaView style={styles.container}>
      <StatusbarView available={account.available} limit={account.limit}></StatusbarView>
      <StatusBar style="auto" />
      {account.transactions.length != 0 ? <TransactionListView transactions={account.transactions}></TransactionListView> : null}
      <Button title='Refresh' onPress={() => {
        fetchData();
      }}></Button>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
});
