import { StyleSheet } from 'react-native';

const statusBarStyle = StyleSheet.create({
    container: {
        height:100,
        flexDirection: 'row',
        justifyContent:'space-evenly',
        alignItems: 'center'
    },
    headerText: {
        color: '#35A2F5',
    },
    mainText: {
        color: '#fff',
        fontSize: 20
    },
    balanceBox : {
        flexDirection: 'column',
        alignItems: 'flex-end'
    }
});

export default statusBarStyle; 