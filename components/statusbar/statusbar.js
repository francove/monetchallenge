import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { View, Text } from 'react-native';
import CurrencyView from '../currency/currency';

import CircleView from './../amount/amount';

import statusBarStyle from './styles';

const StatusbarView = (data) => {

    var ratio = (parseInt(data.available) / parseInt(data.limit));
    var availablePercentage = parseInt(ratio * 100);

    return (
        <LinearGradient colors={['#000', '#1c1c1c', '#000']} locations={[0.7, 0.9, 1]}>
            <View style={statusBarStyle.container}>
                <CircleView ratio={ratio} availablePercentage={availablePercentage}></CircleView>
                <View style={statusBarStyle.balanceBox}>
                    <Text style={statusBarStyle.headerText}>Verfügbar</Text>
                    <CurrencyView amount={data.available}></CurrencyView>
                </View>
                <View style={statusBarStyle.balanceBox}>
                    <Text style={statusBarStyle.headerText}>Limit</Text>
                    <CurrencyView amount={data.limit}></CurrencyView>
                </View>
            </View>
        </LinearGradient>
    );
};

export default StatusbarView;