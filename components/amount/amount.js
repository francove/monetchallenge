import React from 'react';
import { View, Text } from 'react-native';
import Svg, { Circle } from 'react-native-svg';

import circleStyles from './styles';

const CircleView = (data) => {
    const ratio = 1 - data.ratio.toFixed(2);
    const availablePercentage = data.availablePercentage;
    const size = 50;
    const strokeWidth = 5;
    const radius = size / 2 - strokeWidth / 2;
    const circumference = radius * 2 * Math.PI;
    const progressOffset = ratio * circumference;

    return (
        <View style={circleStyles.container}>
            <Svg style={circleStyles.svgCanvas}>
                <Text style={circleStyles.innerText}>{availablePercentage}%</Text>
                <Circle stroke='#fff' r={radius} cx={size / 2} cy={size / 2} fill='none' strokeWidth={strokeWidth} />
                <Circle strokeDashoffset={progressOffset} strokeDasharray={circumference} stroke='#35A2F5' r={radius} cx={size / 2} cy={size / 2} fill='none' strokeWidth={strokeWidth} />
            </Svg>
        </View>
    );
};

export default CircleView;