import { StyleSheet } from 'react-native';

const circleStyles = StyleSheet.create({
    container: {
        width:50,
        height:50,
    },
    svgCanvas: {
        justifyContent:'center',
        alignItems: 'center',
    },
    innerText: {
        height: 50,
        width: 50,
        textAlign: 'center',
        lineHeight: 50,
        color: '#ffff',
    }
});

export default circleStyles; 