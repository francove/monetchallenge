import React from 'react';
import { View, Text } from 'react-native';

import currencyStyles from './styles';

const CurrencyView = (amount) => {
    var euroAmount = amount.amount.split('.');
    return (
            <View style={currencyStyles.container}>
                <Text style={currencyStyles.mainText}>{euroAmount[0]}</Text>
                <View style={currencyStyles.middleContainer}>
                    <Text style={{color: '#fff', fontSize:10}}>{euroAmount[1]}</Text>
                    <Text style={{color: '#fff',fontSize: 20, height:15, textAlignVertical: 'top', lineHeight:10 }}>,</Text>
                </View>
                <Text style={currencyStyles.mainText}>€</Text>
            </View>
    );
};

export default CurrencyView;