import { StyleSheet } from 'react-native';

const currencyStyles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    mainText:{
        fontSize:20,
        color: '#fff'
    },
    middleContainer: {
        flexDirection: 'column',
    }
});

export default currencyStyles; 