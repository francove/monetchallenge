import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import transactionTileStyles from './styles';
import CurrencyView from '../currency/currency';

const TransactionTile = (data) => {
    return(
        <View style={transactionTileStyles.container}>
            <View style={transactionTileStyles.leading}>
                <Ionicons style={{paddingRight:5}} name="swap-horizontal-outline" size={24} color="#35A2F5"/>
                <View style={transactionTileStyles.innerLeading}>
                    <Text style={{color:'#fff'}}>{data.transaction.name}</Text>
                    <Text style={{color:'#fff'}}>{data.transaction.date}</Text>
                </View>
            </View>
            <View style={transactionTileStyles.trailing}>
                <Text style={{color:'#fff'}}>-</Text>
                <CurrencyView amount={data.transaction.amount} ></CurrencyView>
            </View>
        </View>
    );
}

export default TransactionTile;