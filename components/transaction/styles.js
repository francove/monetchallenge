import { StyleSheet } from 'react-native';

const transactionTileStyles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 2,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: '#1c1c1c',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems: 'center'
    },
    leading: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    innerLeading: {
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    trailing: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default transactionTileStyles; 