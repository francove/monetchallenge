import React, { useState } from 'react';
import { View, Text, ScrollView } from 'react-native';
import TransactionTile from '../transaction/transaction';

import transactionListStyles from './styles';

const TransactionListView = (data) => {
    return (
        <View style={transactionListStyles.container}>
            <View style={transactionListStyles.header}>
                <Text style={transactionListStyles.textLeading}>Letzte Umsätze</Text>
                <Text style={transactionListStyles.textTrailing}>EUR</Text>
            </View>
            <ScrollView>
                {data.transactions.length && (
                    data.transactions.map(transaction => <TransactionTile key={transaction.name} transaction={transaction}></TransactionTile>
                        )
                )}
            </ScrollView>
        </View>
    );
};

export default TransactionListView;