import { StyleSheet } from 'react-native';

const transactionListStyles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15
    },
    header: {
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    textLeading: {
        color: '#fff',
        fontSize: 18,
        paddingLeft: 20
    },
    textTrailing: {
        color: '#A0A0A0',
        paddingRight: 20
    }
});

export default transactionListStyles; 